<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Project logo"></a>
</p>

<h3 align="center">Cleanup Backup Files</h3>

---

<p align="center"> Application clean old files acording to filename pattern
      <br> 
        Application clean old files acording to filename pattern <br>
        Usage: python3 cleanup_files.py [-x REGEX_FILE_NAME] [-s START_FILE_NAME] [-e END_FILE_NAME] -n NUMBER_OF_FILES_TO_KEEP -p FILES_PATH [-?] <br>
<br>
                -x : file name regular expression pattern, optional<br>
                -s : files name starting with, optional<br>
                -e : files name ending with, optional<br>
                -n : number of files to keep, mandatory. If there are more than this number of files that the name matches, the oldests files will be deleted<br>
                -p : files path (must be an directory), mandatory<br>
                -? : display this help.<br>
<br>
                Example: python3 cleanup_files.py -x \w*_\d{8}.dump.tgz -n 30 -p /mnt/dietpi_userdata/<br>
</p>
