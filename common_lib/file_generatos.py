#!/usr/bin/python3
# -*- coding: UTF-8 -*-
# import datetime
import os, sys
from datetime import timedelta, datetime, date
from common_lib.common_error import BadUserInputError
from common_lib.files_util import FileUtils
import time
import shutil
import tempfile
import math
import calendar


class FileGenerator(FileUtils):
    def __init__(self,
                    end_date: datetime,
                    dirpath:str,
                    hourly: tuple[int, int] = (1, 0), # (files_hourly, hours)
                    daily: tuple[int, int] = (1, 0), # (files_daily, days)
                    weekly: tuple[int, int] = (1, 0), # (files_weekly, weeks) 
                    monthly: tuple[int, int] = (1, 0), #(files_monthly, months)
                    yearly: tuple[int, int] = (1, 0) # (files_yearly, years),
                    
                ) -> None:
        """
            Weeks are Monday 00:00 to Sunday 23:59, days 00:00 to 23:59, hours :00 to :59, etc
        """
        
        self.dirpath = dirpath
        is_directory = os.path.isdir(self.dirpath)
        if not is_directory:
            raise BadUserInputError(f"path {self.dirpath} is not a directory")
        
        
        # self.end_date = end_date
        
        self.hourly = hourly
        self.daily= daily
        self.weekly = weekly
        self.monthly = monthly
        self.yearly = yearly
        self.validate_input()
        self.generated_files:dict[str, datetime] = {}
        # self.start_date: datetime = self.__get_start_date()
        FileUtils.__init__(self, 
                           end_date=end_date,
                           hours=self._get_hours(),
                           days=self._get_days(),
                           weeks=self._get_weeks(),
                           months=self._get_months(),
                           years=self._get_years()
                           )
    
    def validate_input(self):
        if not (
                (self._get_hours() > 0 and self._get_files_hourly()>0) or 
                (self._get_days() > 0 and self._get_files_daily() > 0) or 
                (self._get_weeks() > 0 and self._get_files_weekly() > 0) or
                (self._get_months() > 0 and self._get_files_monthly() > 0) or
                (self._get_years() > 0 and self._get_files_yearly() > 0)
           ):        
            raise BadUserInputError("""At least one of the parametar of periodicity is requeired.
                                        hourly --> tuple[int, int] = (1, 0), # (files_hourly, hours)
                                        daily --> tuple[int, int] = (1, 0), # (files_daily, days)
                                        weekly --> tuple[int, int] = (1, 0), # (files_weekly, weeks) 
                                        monthly --> tuple[int, int] = (1, 0), #(files_monthly, months)
                                        yearly --> tuple[int, int] = (1, 0) # (files_yearly, years),
                                    """)
    
    def __get_period_number(self, period:tuple[int, int])-> int:
        if period is not None:
            _, period_number = period
        
        return 0 if period_number is None else period_number
    
    def __get_files_number(self, period:tuple[int, int])-> int:
        if period is not None:
            files_number, _ = period
        
        return 0 if files_number is None else files_number
    
    
    def _get_hours(self)-> int:
       return self.__get_period_number(period=self.hourly) 
    
    def _get_days(self)-> int:
       return self.__get_period_number(period=self.daily) 

    def _get_weeks(self)-> int:
       return self.__get_period_number(period=self.weekly) 
   
    def _get_months(self)-> int:
       return self.__get_period_number(period=self.monthly) 
   
    def _get_years(self)-> int:
       return self.__get_period_number(period=self.yearly) 

    
    def _get_files_hourly(self)-> int:
       return self.__get_files_number(period=self.hourly) 
    
    def _get_files_daily(self)-> int:
       return self.__get_files_number(period=self.daily) 

    def _get_files_weekly(self)-> int:
       return self.__get_files_number(period=self.weekly) 
   
    def _get_files_monthly(self)-> int:
       return self.__get_files_number(period=self.monthly) 
   
    def _get_files_yearly(self)-> int:
       return self.__get_files_number(period=self.yearly) 

    # def __get_start_date(self) -> datetime :
    #     start_date: datetime = self.end_date
    #     if self._get_days() > 0:
    #         start_date = self.end_date - timedelta(days=self._get_days())
                
    #     if self._get_weeks() > 0:
    #         new_weekly_day = self.end_date - timedelta(weeks=self._get_weeks())
    #         if new_weekly_day < start_date:
    #             start_date = new_weekly_day
            
    #     if self._get_months() > 0:
    #         new_monthly = self.end_date - timedelta(days=self._get_months()*365/12)
    #         if new_monthly < start_date:
    #             start_date = new_monthly
        
    #     if self._get_years() > 0:
    #         new_yearly = self.end_date - timedelta(days=self._get_years()*365)
    #         if new_yearly < start_date:
    #             start_date = new_yearly
                
    #     return start_date
            
    
    def total_genetated_files(self) -> int:
        return len(self.generated_files)
    
    def __mktime(self, value:tuple[str, datetime]):
        _, v = value
        return time.mktime(v.timetuple())
    
    def __filter_files_by_daterange_sorted_desc(self, start_date:datetime, end_date:datetime=None) -> list[tuple[str, datetime]]:
        return_list: list[str]= []
        
        if start_date is not None: 
            end_date = start_date if end_date is None else end_date
        
        for k, v in self.generated_files.items():
            if  v >= start_date and v <= end_date :
                return_list.append((k, v))
        return sorted(return_list, key=self.__mktime, reverse=True) 
                
    # def _get_yearly_datetime_range(self):
    #     for n in range(self._get_years() + 1):
    #         days: int =  self.end_date.timetuple().tm_yday if  n == 0 else ((n*365)  - self.end_date.timetuple().tm_yday)
    #         yield self.end_date - timedelta(days=days - 1)
        
    # def _get_monthly_datetime_range(self):
    #     for n in range(self._get_months() + 1):
    #         days: int =   self.end_date.day if  n == 0 else ((n*365/12)  - self.end_date.day)
    #         yield self.end_date - timedelta(days=days - 1)
  
    # def _get_weekly_datetime_range(self):
    #     for n in range(self._get_weeks() + 1):
    #         yield self.end_date - timedelta(days=(n*7 + self.end_date.weekday()))
  
    # def _get_daily_datetime_range(self):
    #     for n in range(self._get_days() + 1):
    #         yield self.end_date - timedelta(days=n)

    # def _get_hourly_datetime_range(self):
    #     for n in range(self._get_hours()):
    #         yield self.end_date + timedelta(hours=n)

    def __generate_files(self, end_datetime:datetime, number_of_file:int, seconds_interval_between_files:int, prefix:str) -> None:
        # modTime = time.mktime(date_time.timetuple())
        # Get the desired creation and modification datetime
        creation_datetime_epoch = time.mktime(end_datetime.timetuple()) 
        # January 1, 2022, 10:30 AM
        for n in range(number_of_file): #loop files hourly
            tf = tempfile.NamedTemporaryFile(delete=False, prefix=f"{prefix}_")
            # Specify the file path
            # file_path = pathlib.Path(tf.name)
            # file_path.touch()
            # file_path.stat().st_ctime = creation_datetime.timestamp()
            # file_path.stat().st_mtime = modification_datetime.timestamp()
            creation_datetime = datetime.fromtimestamp(creation_datetime_epoch)
            file_name:str = f"{prefix}-BKP-{creation_datetime.strftime('%Y%m%d-%H%M%S')}.tar.gz" 
            new_path = os.path.join(self.dirpath, os.path.basename(file_name))
            shutil.move(tf.name, new_path)
            os.utime(new_path, (creation_datetime_epoch, creation_datetime_epoch))
            self.generated_files[new_path] = creation_datetime
            creation_datetime_epoch += seconds_interval_between_files #sum seconds interval between files
    
    def __generate_yearly_files(self) -> None:
        '''
            Generate yearly file(s) from end_date backwards  for the number of years defined on class constructor
        '''
        year = self._get_years()
        files_yearly = self._get_files_yearly()
        if year > 0 and files_yearly > 0:
            yearly_count = 0
            for first_day_of_year in self._get_yearly_datetime_range(): #loop dates
                yearly_count+=1
                last_year_day_datetime:datetime =  self._get_end_of_day_from_datetime(date_time=datetime(year=first_day_of_year.year, 
                                                                                                         month=12,
                                                                                                         day=31) #get last day of year
                                                                                    )
                last_year_day_datetime = self.end_date if last_year_day_datetime > self.end_date else last_year_day_datetime
                self.__generate_files_to_period(start_date=first_day_of_year, 
                                                        end_date=last_year_day_datetime, 
                                                        files_per_period=files_yearly,
                                                        seconds_interval_between_files=60,
                                                        files_prefix=f"yearly_{yearly_count}") 

    def __generate_monthly_files(self) -> None:
        '''
            Generate monthly file(s) from end_date backwards  for the number of months defined on class constructor
        '''
        months = self._get_months()
        files_monthly = self._get_files_monthly()
        if months > 0 and files_monthly > 0:
            monthly_count = 0
            for fisrt_month_day in self._get_monthly_datetime_range(): #loop dates
                monthly_count+=1
                (_, last_day_of_month) = calendar.monthrange(fisrt_month_day.year, fisrt_month_day.month)
                last_month_day =  self._get_end_of_day_from_datetime(date_time=datetime(year=fisrt_month_day.year, 
                                                                                        month= fisrt_month_day.month, 
                                                                                        day=last_day_of_month) #get last day of month
                                                                    )
                last_month_day = self.end_date if last_month_day > self.end_date else last_month_day
                self.__generate_files_to_period(start_date=fisrt_month_day, 
                                                        end_date=last_month_day, 
                                                        files_per_period=files_monthly,
                                                        seconds_interval_between_files=60,
                                                        files_prefix=f"monthly_{monthly_count}")              

    def __generate_weekly_files(self) -> None:
        '''
            Generate weekly file(s) from end_date backwards for the number of weeks defined on class constructor
        '''
        weeks = self._get_weeks()
        files_weekly = self._get_files_weekly()
        if weeks > 0 and files_weekly > 0:
            weekly_count = 0
            for start_week_day in self._get_weekly_datetime_range(): #loop dates
                weekly_count+=1
                end_week_day:datetime = start_week_day + timedelta(days=6, hours=23, minutes=59, seconds=59) 
                end_week_day = end_week_day if end_week_day < self.end_date else self.end_date
                self.__generate_files_to_period(start_date=start_week_day, 
                                                        end_date=end_week_day, 
                                                        files_per_period=files_weekly,
                                                        seconds_interval_between_files=60,
                                                        files_prefix=f"weekly_{weekly_count}")

    def __generate_daily_files(self) -> None:
        '''
            Generate daily file(s) from end_date backwards for the number of days defined on class constructor
        '''
        days = self._get_days()
        files_daily = self._get_files_daily()
        if days > 0 and files_daily > 0:
            daily_count = 0
            for start_datetime in self._get_daily_datetime_range(): #loop dates
                daily_count+=1
                end_datetime = self._get_end_of_day_from_datetime(start_datetime)
                self.__generate_files_to_period(start_date=start_datetime, 
                                                        end_date=end_datetime, 
                                                        files_per_period=files_daily,
                                                        seconds_interval_between_files=60,
                                                        files_prefix=f"daily_{daily_count}")

            
            
               

    def __generate_hourly_files(self) -> None:
        '''
            Generate hourly file(s) on end_date for the number of hours defined on class constructor
        '''
        hours:int = self._get_hours()
        files_hourly: int = self._get_files_hourly() 
        
        if hours > 0 and files_hourly > 0:
            seconds_interval_between_files: int = math.floor(3599 / files_hourly)
            hourly_count = 0
            for start_datetime_hour in self._get_hourly_datetime_range(): #loop dates
                hourly_count+=1
                end_datetime_hour = start_datetime_hour  + timedelta(minutes=59, seconds=59) 
                self.__generate_files_to_period(start_date=start_datetime_hour, 
                                                        end_date=end_datetime_hour, 
                                                        files_per_period=files_hourly,
                                                        seconds_interval_between_files=seconds_interval_between_files,
                                                        files_prefix=f"hourly_{hourly_count}")
                
        
    def __generate_files_to_period(self, files_per_period:int, start_date:datetime, end_date:datetime, seconds_interval_between_files:int, files_prefix:str) -> None :
        desc_sorted_file_list = self.__filter_files_by_daterange_sorted_desc(start_date=start_date, end_date=end_date)
        number_of_files_alread_exists_at_date:int = len(desc_sorted_file_list)
        number_file_to_generate = files_per_period - number_of_files_alread_exists_at_date
        start_date_time:datetime = start_date
        if number_file_to_generate > 0:
            if number_of_files_alread_exists_at_date > 0:
                _, start_date_time = desc_sorted_file_list[0]  #get most recent file  
                start_date_time = start_date_time + timedelta(seconds=60)     
                
            self.__generate_files(end_datetime=start_date_time,
                                            number_of_file=number_file_to_generate, 
                                            seconds_interval_between_files=seconds_interval_between_files,
                                            prefix=files_prefix)
               
    def generate_files(self) -> None:
        self.__generate_hourly_files()
        self.__generate_daily_files()
        self.__generate_weekly_files()
        self.__generate_monthly_files()
        self.__generate_yearly_files()
                