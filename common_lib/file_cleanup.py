import os
from datetime import datetime, timedelta
from common_lib.common_error import BadUserInputError
from common_lib.files_util import FileUtils
import re
from pathlib import Path
import calendar

class FileCleanup(FileUtils):
    def __init__(self,
                    file_pattern:str,
                    dirpath:str,
                    keep_last:int = 30,
                    keep_hourly:int = 0,
                    keep_daily:int = 0,
                    keep_weekly:int = 0,
                    keep_monthly:int = 0,
                    keep_yearly:int = 0,
                    start_files_name:str= None,
                    end_files_name:str= None, 
                    start_files_datetime:datetime = None,
                    end_files_datetime:datetime = None
                 ) -> None:
      
        
        self.file_pattern = file_pattern
        self.dirpath = dirpath
        self.keep_last = keep_last
        self.keep_hourly = keep_hourly
        self.keep_daily = keep_daily
        self.keep_weekly = keep_weekly
        self.keep_monthly = keep_monthly
        self.keep_yearly = keep_yearly
        self.start_files_name = start_files_name
        self.end_files_name = end_files_name
        self.start_files_datetime = start_files_datetime
        self.end_files_datetime = end_files_datetime
        
        FileUtils.__init__(self, 
                           end_date=datetime.now() if self.end_files_datetime is None else self.end_files_datetime,
                           hours=keep_hourly,
                           days=keep_daily,
                           weeks=keep_weekly,
                           months=keep_monthly,
                           years=keep_yearly)
        
          #checks if path is a directory
        is_directory = os.path.isdir(dirpath)

        if not is_directory:
            raise BadUserInputError(f"The path {dirpath} is not a directory")
        
        
    
        # call listdir() method
        # path is a directory of which you want to list
        self.__mached_files_to_delete:list[str] = []
        self.files_to_delete:list[str] = []
        self.deleted_files:list[str] = []
        self.files_to_keep:list[str] = []
        self.match_files_to_delete()
        self.match_files_to_keep()
        
    
    def match_files_to_delete(self) -> None:  
        mached_files_to_delete:list[str] = []
        mached_files_to_delete = sorted(Path(self.dirpath).iterdir(), key=os.path.getmtime, reverse=True)  
        #remove directories from matched_files
        mached_files_to_delete = [x for x in mached_files_to_delete if not os.path.isdir(x)]

        if self.start_files_name :
            mached_files_to_delete = [x for x in mached_files_to_delete if x.name.startswith(self.start_files_name)]

        if self.end_files_name :
            mached_files_to_delete = [x for x in mached_files_to_delete if x.name.endswith(self.end_files_name)]
                
        if self.file_pattern:
            full_file_pattern = os.path.join(self.dirpath, self.file_pattern)
            mached_files_to_delete = [f.as_posix() for f in mached_files_to_delete]
            r = re.compile(full_file_pattern)
            mached_files_to_delete = list(filter(r.match, mached_files_to_delete)) # Read


        if self.start_files_datetime:            
           mached_files_to_delete = [x for x in mached_files_to_delete if datetime.fromtimestamp(os.path.getmtime(x)) >= self.start_files_datetime]

        if self.end_files_datetime:            
            mached_files_to_delete = [x for x in mached_files_to_delete if datetime.fromtimestamp(os.path.getmtime(x)) <= self.end_files_datetime]
        
        self.__mached_files_to_delete.extend(list(dict.fromkeys(mached_files_to_delete)))
        self.files_to_delete.extend(self.__mached_files_to_delete)
                

    def __get_files_to_be_keeped(self, start_datetime:datetime, end_datetime:datetime) -> list[str]:
        return [x for x in self.__mached_files_to_delete if datetime.fromtimestamp(os.path.getmtime(x)) >= start_datetime and datetime.fromtimestamp(os.path.getmtime(x)) <= end_datetime ]

    def add_list_to_keep(self, start_datetime:datetime, end_datetime:datetime):
        list_to_keep = self.__get_files_to_be_keeped(start_datetime=start_datetime, end_datetime=end_datetime)
        if len(list_to_keep) > 0:
            if list_to_keep[0] not in self.files_to_keep:
                self.files_to_keep.append(list_to_keep[0])
            else:
                print(f"file {list_to_keep[0]} alread exists on keep file list")

    def match_files_to_keep(self):
        #keep_hourly files
        total_matched_files_to_delete = len(self.__mached_files_to_delete)
        
        if not self.keep_last or (total_matched_files_to_delete > self.keep_last):
            if (self.keep_last):
                self.files_to_keep.extend(self.__mached_files_to_delete[0:self.keep_last])
        
        if self.keep_hourly is not None and self.keep_hourly > 0:
            for start_datetime_hour_to_keep in self._get_hourly_datetime_range(): #loop dates
                end_datetime_hour_to_keep = start_datetime_hour_to_keep  + timedelta(minutes=59, seconds=59) 
                self.add_list_to_keep(start_datetime=start_datetime_hour_to_keep, end_datetime=end_datetime_hour_to_keep)

        if self.keep_daily is not None and self.keep_daily > 0:
            for start_date_time_to_keep in self._get_daily_datetime_range(): #loop dates
                end_date_time_to_keep = self._get_end_of_day_from_datetime(start_date_time_to_keep)
                self.add_list_to_keep(start_datetime=start_date_time_to_keep, end_datetime=end_date_time_to_keep)

        if self.keep_weekly is not None and self.keep_weekly > 0:
            for start_week_day_to_keep in self._get_weekly_datetime_range(): #loop dates
                end_week_day_to_keep:datetime = start_week_day_to_keep + timedelta(days=6, hours=23, minutes=59, seconds=59) 
                end_week_day_to_keep = end_week_day_to_keep if end_week_day_to_keep < self.end_date else self.end_date
                self.add_list_to_keep(start_datetime=start_week_day_to_keep, end_datetime=end_week_day_to_keep)

        if self.keep_monthly is not None and self.keep_monthly > 0:
             for fisrt_month_day_to_keep in self._get_monthly_datetime_range(): #loop dates
                (_, last_day_of_month) = calendar.monthrange(fisrt_month_day_to_keep.year, fisrt_month_day_to_keep.month)
                last_month_day_to_keep = datetime( year=fisrt_month_day_to_keep.year, month= fisrt_month_day_to_keep.month,day=last_day_of_month ) #get last day of month
                last_month_day_to_keep = self.end_date if last_month_day_to_keep > self.end_date else last_month_day_to_keep
                self.add_list_to_keep(start_datetime=fisrt_month_day_to_keep, end_datetime=last_month_day_to_keep)

        if self.keep_yearly is not None and self.keep_yearly > 0:
            for first_day_of_year_to_keep in self._get_yearly_datetime_range(): #loop dates
                last_year_day_datetime_to_keep:datetime =  self._get_end_of_day_from_datetime(date_time=datetime(year=first_day_of_year_to_keep.year, 
                                                                                                         month=12,
                                                                                                         day=31) #get last day of year
                                                                                    )
                last_year_day_datetime_to_keep = self.end_date if last_year_day_datetime_to_keep > self.end_date else last_year_day_datetime_to_keep
                self.add_list_to_keep(start_datetime=first_day_of_year_to_keep, end_datetime=last_year_day_datetime_to_keep)
        
        # self.files_to_delete.extend( list(dict.fromkeys(self.__mached_files_to_delete)))
           
        for keep_file in self.files_to_keep:
            self.files_to_delete.remove(keep_file)
            


    def print_keeped_files(self):
        keeped_count:int  = 0
        for file_to_keep in sorted(self.files_to_keep):
            keeped_count +=  1
            print(f"> {keeped_count} - File {file_to_keep} to be keeped.")

    def print_deleted_files(self):
        # This would print all the files and directories
        delete_count:int = 0
        for deleted_file in sorted(self.deleted_files):
            delete_count += 1
            print(f"> {delete_count} - File {deleted_file} deleted.")
            
    def delete_files(self):
        for file_to_delete in self.files_to_delete:
            os.remove(file_to_delete)
            self.deleted_files.append(file_to_delete) 
            
    def mached_files_to_delete_count(self) -> int:
        return len(self.__mached_files_to_delete)
    
    def deleted_files_count(self) -> int:
        return len(self.deleted_files)
    
    def files_to_keep_count(self) -> int:
        return len(self.files_to_keep)