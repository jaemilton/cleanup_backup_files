from datetime import datetime, timedelta
from common_lib.common_error import BadUserInputError
from typing import Sequence
import calendar

class FileUtils(object):
    def __init__(self,
                 end_date:datetime,
                 hours:int = 0,
                 days:int = 0,
                 weeks:int = 0,
                 months:int = 0,
                 years:int= 0) -> None:
        
        if end_date is None:
            raise BadUserInputError(f"invalid end_date")
        
        self.end_date:datetime = end_date
        self.end_date_begin_of_day = self._get_begin_of_day_from_datetime(date_time=end_date)
        self.end_date_end_of_day = self._get_end_of_day_from_datetime(date_time=end_date)
        self.hours:int = hours
        self.days:int = days
        self.weeks:int = weeks
        self.months:int = months
        self.years:int =years
    
    def _get_begin_of_day_from_datetime(self, date_time:datetime) -> datetime:
        return datetime(year = date_time.year, month=date_time.month, day = date_time.day)
    
    def _get_end_of_day_from_datetime(self, date_time:datetime) -> datetime:
        return datetime(day=date_time.day, month=date_time.month, year=date_time.year,hour=23, minute=59, second=59) 
    
    def __previus_month(self, year, month) -> tuple[int, int]:
        if month == 1:
            return (year-1, 12)
        else:
            return (year, month-1)
    
    def _get_yearly_datetime_range(self):
        for n in range(self.years):
            year: int =  self.end_date.year - n
            yield  datetime(day=1, month=1, year=year)
        
    def _get_monthly_datetime_range(self):
        for n in range(self.months):
            if n == 0:
                month = self.end_date_end_of_day.month
                year = self.end_date_end_of_day.year
            else:
                year = self.end_date_end_of_day.year
                month = self.end_date_end_of_day.month
                for _ in range(n):
                    (year, month) = self.__previus_month(year=year, 
                                                         month=month)
            
            yield datetime(day=1, month=month, year=year)
  
    def _get_weekly_datetime_range(self):
        for n in range(1, self.weeks + 1):
            days: int = self.end_date.weekday() + 1 if n == 1 else (n*7 - (7-self.end_date.weekday()) + 1)
            yield self.end_date_end_of_day - timedelta(days=days)  + timedelta(seconds=1)
  
    def _get_daily_datetime_range(self):
        for n in range(1, self.days + 1):
            yield self.end_date_end_of_day - timedelta(days=n) + timedelta(seconds=1)

    def _get_hourly_datetime_range(self):
        for n in range(1, self.hours + 1):
            yield self.end_date_end_of_day - timedelta(hours=n) + timedelta(seconds=1)