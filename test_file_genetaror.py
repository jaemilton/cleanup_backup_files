import os, sys
import time
from common_lib.common_base import get_input_parameter_value, valid_mandatory_parameters
from common_lib.file_generatos import FileGenerator
from common_lib.common_error import BadUserInputError  
import datetime
import tempfile
from datetime import timedelta, datetime
import shutil
import math


DATETIME_FORMAT="%Y-%m-%d %H:%M:%S"



def get_files_param(input_param:str)-> tuple[int,int]:
    """
        Get a tuple with two int values
        first -> number of files that must be genetared
        secound -> number of interval (hours or days or or weeks or months or years)
    """
    return_value:  tuple[int,int]
    if input_param is not None:
        input_param_splited = input_param.split(';')
        if len(input_param_splited) == 1:
            return_value = (1, 0 if input_param_splited[0] is None else int(input_param_splited[0]))  
        else:
            return_value = (1 if input_param_splited[0] is None else int(input_param_splited[0]), 0 if input_param_splited[1] is None else int(input_param_splited[1]))  
        
        return return_value    
    else:
        return (0, 0)
    
    

def start(argv):
    if '-?' in argv:
        print("""Application to generate ramdom files with past date
        Usage: python3 test_file_genetaror.py -n NUMBER_OF_FILES_PER_DAY -s START_DATE -e END_DATE -p FILES_PATH [-?] 

                -e : end date of files with format %Y-%m-%d, mandatory
                -p : files path (must be an directory), mandatory
                -files_hourly [f;]n : create f files hourly for the last n hours, if f not set, assume f=1.
                -files_daily [f;]n : create f files daily for the last n days, if f not set, assume f=1.
                -files_weekly [f;]n : create f files weekly for the last n weeks, if f not set, assume f=1.
                -files_monthly [f;]n : create f files monthly for the last n months, if f not set, assume f=1.
                -files_yearly [f;]n: create f files yearly for the last n years, if f not set, assume f=1.
                -? : display this help.

                Example: python3 test_file_genetaror.py -files_daily 2;5 -s '2022-02-27' -e '2022-03-07' -p /mnt/dietpi_userdata/
                """)
    elif not valid_mandatory_parameters(argv, ['-p', '-e']):
        raise BadUserInputError(
            """Input error. To run, call as python3 test_file_genetaror.py /
                -e END_DATE /
                -p FILES_PATH /
                [-files_hourly NUMBER_OF_FILES_HOURLY on end date]
                [-files_daily NUMBER_OF_FILES_PER_DAY]
                [-files_daily NUMBER_OF_FILES_PER_DAY]
                
                [-files_daily NUMBER_OF_FILES_PER_DAY]
                [-files_daily NUMBER_OF_FILES_PER_DAY]
                [-?] """)

    else:
        # start_date:datetime = datetime.strptime(get_input_parameter_value(argv, '-s'), DATETIME_FORMAT)
        end_date:datetime = datetime.strptime(f"{get_input_parameter_value(argv, '-e')} 23:59:59", DATETIME_FORMAT)
        dirpath = get_input_parameter_value(argv, '-p')
        
        
        file_generator = FileGenerator(end_date = end_date,
                                        hourly =  get_files_param(get_input_parameter_value(argv, '-files_hourly')) ,
                                        daily = get_files_param(get_input_parameter_value(argv, '-files_daily')) ,
                                        weekly = get_files_param( get_input_parameter_value(argv, '-files_weekly')) ,
                                        monthly =  get_files_param(get_input_parameter_value(argv, '-files_monthly')) ,
                                        yearly = get_files_param(get_input_parameter_value(argv, '-files_yearly')),
                                        dirpath = dirpath
                                        )
       
        file_generator.generate_files()
        print(f">>> Total genetated files {file_generator.total_genetated_files()}.")        
    


if __name__ == '__main__':
    start(sys.argv)
