#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os, sys
from common_lib.common_base import get_input_parameter_value, valid_mandatory_parameters
from common_lib.common_error import BadUserInputError
import re
from pathlib import Path
from datetime import datetime, date, timedelta
from common_lib.file_cleanup import FileCleanup


DATE_FORMAT="%Y-%m-%d %H:%M:%S"
def get_keep_files_param(argv, param_name) -> int:
    keep_files_param = None
    keep_files_param_aux = get_input_parameter_value(argv, param_name)
    if keep_files_param_aux:
        keep_files_param = int(keep_files_param_aux)
    return keep_files_param





def start(argv):
    if '-?' in argv:
        print("""Application to clean old files acording to filename pattern
        Usage: python3 cleanup_files.py [-x REGEX_FILE_NAME] [-sn START_FILE_NAME] [-en END_FILE_NAME] [-sd START_FILE_DATE_TIME] [-ed END_FILE_DATE_TIME] [-n NUMBER_OF_FILES_TO_KEEP] -p FILES_PATH [-?] 

                -x : file name regular expression pattern, optional
                -sn : files name starting with to be deleted, optional
                -en : files name ending with to be deleted, optional
                -sd : files starting datetime with format %Y-%m-%d %H:%M:%S to be deleted, optional, all files with creation datetime grater then this datetime will be deleted
                -ed : files ending datetime with format %Y-%m-%d %H:%M:%S to be deleted, optional, all files with creation datetime lower then this datetime will be deleted
                -keep_last n : keep the n last (most recent)
                -keep_hourly n : for the last n hours which have one or more files, keep only the most recent one for each hour.
                -keep_daily n : for the last n days which have one or more files, keep only the most recent one for each day.
                -keep_weekly n : for the last n weeks which have one or more files, keep only the most recent one for each week.
                -keep_monthly n : for the last n months which have one or more files, keep only the most recent one for each month.
                -keep-yearly n : for the last n years which have one or more files, keep only the most recent one for each year.
                -p : files path (must be an directory), mandatory
                -? : display this help.

                Example: python3 cleanup_files.py -x \w*_\d{8}.dump.tgz -n 30 -p /mnt/dietpi_userdata/
                """)
    elif not valid_mandatory_parameters(argv, ['-p']):
        raise BadUserInputError(
            """Input error. To run, call as python3 cleanup_files.py [-x REGEX_FILE_NAME] \
                [-sn START_FILE_NAME] \
                [-en END_FILE_NAME] \
                [-sd START_FILE_DATE_TIME] \
                [-ed END_FILE_DATE_TIME] \
                [-d NUMBER_OF_DAYS_FROM_TODA] \
                [-n NUMBER_OF_FILES_TO_KEEP] \
                [-keep_last n] \
                [-keep_hourly n] \
                [-keep_daily n] \
                [-keep_weekly n] \
                [-keep_monthly n] \
                [-keep_yearly n] \
                -p FILES_PATH \
                [-?]""")

    else:
        dirpath = get_input_parameter_value(argv, '-p')
        file_pattern = get_input_parameter_value(argv, '-x')
        keep_last = get_keep_files_param(argv=argv, param_name='-keep_last')
        keep_hourly = get_keep_files_param(argv=argv, param_name='-keep_hourly')
        keep_daily = get_keep_files_param(argv=argv, param_name='-keep_daily')
        keep_weekly = get_keep_files_param(argv=argv, param_name='-keep_weekly')
        keep_monthly = get_keep_files_param(argv=argv, param_name='-keep_monthly')
        keep_yearly = get_keep_files_param(argv=argv, param_name='-keep_yearly')
            
        
        start_files_name = get_input_parameter_value(argv, '-sn')
        end_files_name = get_input_parameter_value(argv, '-en')
        
        start_files_datetime_aux = get_input_parameter_value(argv, '-sd')
        end_files_datetime_aux = get_input_parameter_value(argv, '-ed')

        start_files_datetime = None
        if start_files_datetime_aux:
            start_files_datetime = datetime.strptime(start_files_datetime_aux, DATE_FORMAT)

        end_files_datetime = None
        if end_files_datetime_aux:
            end_files_datetime = datetime.strptime(end_files_datetime_aux, DATE_FORMAT)


        file_cleanup:FileCleanup = FileCleanup(file_pattern = file_pattern,
                                                dirpath = dirpath,
                                                keep_last = keep_last,
                                                keep_hourly = keep_hourly,
                                                keep_daily = keep_daily,
                                                keep_weekly = keep_weekly,
                                                keep_monthly = keep_monthly,
                                                keep_yearly = keep_yearly,
                                                start_files_name = start_files_name,
                                                end_files_name = end_files_name, 
                                                start_files_datetime = start_files_datetime,
                                                end_files_datetime = end_files_datetime)

        file_cleanup.delete_files()
        
        file_cleanup.print_deleted_files()
        print(f">>>>>>>>>>>>>>>>>>>----------------------------------------<<<<<<<<<<<<<<<<<<<<")
        file_cleanup.print_keeped_files()
        
        print(f">>>>>>>>>>>>>>>>>>>----------------------------------------<<<<<<<<<<<<<<<<<<<<")
        print(f">>> Total of mached files {file_cleanup.mached_files_to_delete_count()}.")
        print(f">>> Total of keeped files {file_cleanup.files_to_keep_count()}.")
        print(f">>> Total of deleted files {file_cleanup.deleted_files_count()}.")
        
        # #checks if path is a directory
        # is_directory = os.path.isdir(dirpath)

        # if is_directory:
        #     # call listdir() method
        #     # path is a directory of which you want to list
        #     matched_files_to_delete = sorted(Path(dirpath).iterdir(), key=os.path.getmtime)

        #     #remove directories from matched_files
        #     matched_files_to_delete = [x for x in matched_files_to_delete if not os.path.isdir(x)]

        #     if start_files_name :
        #         matched_files_to_delete = [x for x in matched_files_to_delete if x.name.startswith(start_files_name)]

        #     if end_files_name :
        #         matched_files_to_delete = [x for x in matched_files_to_delete if x.name.endswith(end_files_name)]
                    
        #     if file_pattern:
        #         full_file_pattern = os.path.join(dirpath, file_pattern)
        #         matched_files_to_delete = [f.as_posix() for f in matched_files_to_delete]
        #         r = re.compile(full_file_pattern)
        #         matched_files_to_delete = list(filter(r.match, matched_files_to_delete)) # Read


        #     if start_files_datetime:            
        #         matched_files_to_delete = [x for x in matched_files_to_delete if datetime.fromtimestamp(os.path.getmtime(x)) >= start_files_datetime]

        #     if end_files_datetime:            
        #         matched_files_to_delete = [x for x in matched_files_to_delete if datetime.fromtimestamp(os.path.getmtime(x)) <= end_files_datetime]

        # else:
        #     raise BadUserInputError(f"The path {dirpath} is not a directory")

        # total_matched_files_to_delete = len(matched_files_to_delete)
        # if not keep_last or (total_matched_files_to_delete > keep_last):
        #     if (keep_last):
        #         files_to_delete = matched_files_to_delete[0:(total_matched_files_to_delete-keep_last)]
        #     else:
        #         files_to_delete = matched_files_to_delete

        #     matched_files_to_keep: list[str] = []
        #     #keep_hourly files
            
        #     if keep_hourly is not None and keep_hourly > 0:
        #         start_date_time_to_keep: datetime = datetime.now() - timedelta(hours= keep_hourly)
        #         matched_files_to_keep.extend([x for x in matched_files_to_delete if datetime.fromtimestamp(os.path.getmtime(x)) >= start_date_time_to_keep][:keep_hourly])

        #     if keep_daily is not None and keep_daily > 0:
        #         for n in range(keep_daily):
                    
        #             start_date_time_to_keep: datetime = datetime.now() - timedelta(days = n)
        #             end_date_time_to_keep: datetime = datetime.now() - timedelta(days = n)
        #             list_files_to_keep = [x for x in matched_files_to_delete if  datetime.fromtimestamp(os.path.getmtime(x)) >= start_date_time_to_keep and datetime.fromtimestamp(os.path.getmtime(x)) <= end_date_time_to_keep]
        #             if len(list_files_to_keep) > 0:
        #                 matched_files_to_keep.append(list_files_to_keep[0])

        #     if keep_weekly is not None and keep_weekly > 0:
                
        #         start_date_time_to_keep: datetime = datetime.now() - timedelta(weeks= keep_weekly)
        #         matched_files_to_keep.extend([x for x in matched_files_to_delete if datetime.fromtimestamp(os.path.getmtime(x)) >= start_date_time_to_keep][:keep_weekly])

        #     if keep_monthly is not None and keep_monthly > 0:
        #         start_date_time_to_keep: datetime = datetime.now() - timedelta(days= keep_monthly*365/12)
        #         matched_files_to_keep.extend([x for x in matched_files_to_delete if datetime.fromtimestamp(os.path.getmtime(x)) >= start_date_time_to_keep][:keep_monthly])

        #     if keep_yearly is not None and keep_yearly > 0:
        #         start_date_time_to_keep: datetime = datetime.now() - timedelta(days= keep_yearly*365)
        #         matched_files_to_keep.extend([x for x in matched_files_to_delete if datetime.fromtimestamp(os.path.getmtime(x)) >= start_date_time_to_keep][:keep_yearly])

        #     for keep_file in matched_files_to_keep:
        #         files_to_delete.remove(keep_file)

        #     non_deleted_files = [non_deleted for non_deleted in matched_files_to_delete if non_deleted not in files_to_delete]
        #     # This would print all the files and directories
        #     for file_to_delete in files_to_delete:
        #         # os.remove(file_to_delete) 
        #         number_of_delete_files = number_of_delete_files + 1
        #         print(f"> {number_of_delete_files} - File {file_to_delete} deleted.")

        #     print(f">>>>>>>>>>>>>>>>>>>----------------------------------------<<<<<<<<<<<<<<<<<<<<")
        #     for file_not_deleted in non_deleted_files:
        #         number_of_non_delete_files = number_of_non_delete_files + 1
        #         print(f"> {number_of_non_delete_files} - File {file_not_deleted} keeped.")
        # else:
        #     for file_not_deleted in matched_files_to_delete:
        #         number_of_non_delete_files = number_of_non_delete_files + 1
        #         print(f"> {number_of_non_delete_files} - File {file_not_deleted} keeped.")


    # print(f">>> Total of keeped files {number_of_non_delete_files}.")
    # print(f">>> Total of deleted files {number_of_delete_files}.")

    
if __name__ == '__main__':
    start(sys.argv)
